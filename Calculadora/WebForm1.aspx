﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Calculadora.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calculadora</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="lblCalculadora" runat="server" Text="Calculadora" Font-Bold="True" Font-Size="Large"></asp:Label>

            <br />

            <asp:TextBox ID="TxtbNumber1" runat="server" Width="122px" ToolTip="Número 1"></asp:TextBox>
                
            <br />

            <asp:DropDownList ID="ddlOperations" runat="server" Height="19px" Width="131px" ToolTip="Operações"></asp:DropDownList>

            <br />

            <asp:TextBox ID="TxtbNumber2" runat="server" Width="122px" ToolTip="Número 2"></asp:TextBox>

            <br />
            
            <asp:Button ID="btnCalcular" runat="server" Text="Calcular" BackColor="#66FF33" Font-Bold="True" ForeColor="White" OnClick="Button1_Click" Width="129px" ToolTip="Calcular" />

            <br />
            <br />

            <asp:Label ID="lblResultado" runat="server" ToolTip="Resultado"></asp:Label>

       </div>
        
    </form>
</body>
</html>
