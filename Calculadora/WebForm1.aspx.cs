﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculadora
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!IsPostBack)
            {
                //Criar DataTable
                DataTable dt = new DataTable();

                //Adicionar colunas na DataTable
                dt.Columns.Add("operations", typeof(string));
                dt.Columns.Add("value", typeof(int));

                //Adicionar linhas na DataTable
                dt.Rows.Add("", 0);
                dt.Rows.Add("+", 1);
                dt.Rows.Add("-", 2);
                dt.Rows.Add("*", 3);
                dt.Rows.Add("/", 4);

                //Vincular DataTable a DropDownList
                ddlOperations.DataSource = dt;
                ddlOperations.DataTextField = dt.Columns["operations"].ToString();
                ddlOperations.DataValueField = dt.Columns["value"].ToString();
                ddlOperations.DataBind();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            double value1 = Convert.ToDouble(TxtbNumber1.Text);
            double value2 = Convert.ToDouble(TxtbNumber2.Text);
            string selectedOperation = ddlOperations.SelectedItem.Text;
            double resultado;

            switch (selectedOperation)
            {
                case "+":
                    resultado = value1 + value2;
                    break;
                case "-":
                    resultado = value1 - value2;
                    break;
                case "*":
                    resultado = value1 * value2;
                    break;
                case "/":
                    if (value2 != 0) 
                    {
                        resultado = value1 / value2;
                    }
                    else
                    {
                        lblResultado.Text = "Não pode ser divisível por 0.";
                        return; 
                    }
                    break;
                default:
                    lblResultado.Text = "Operação Inválida.";
                    return; 
            }

            lblResultado.Text = "O resultado é: " + resultado;

        }
    }
}